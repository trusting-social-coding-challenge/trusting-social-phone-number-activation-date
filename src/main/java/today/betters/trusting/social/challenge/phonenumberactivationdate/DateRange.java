package today.betters.trusting.social.challenge.phonenumberactivationdate;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateRange implements Comparable {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private Date start;
    private Date end;

    public DateRange(Date start) {
        this.start = start;
    }

    public DateRange(Date start, Date end) {
        this.start = start;
        this.end = end;
    }

    public Date end() {
        return end;
    }

    public Date start() {
        return start;
    }

    public boolean includes(Date arg) {
        return !arg.before(start) && (null == end || !arg.after(end));
    }

    public boolean equals(Object arg) {
        if (!(arg instanceof DateRange)) return false;
        DateRange other = (DateRange) arg;
        return start.equals(other.start) && end.equals(other.end);
    }

    public int hashCode() {
        return start.hashCode();
    }

    @Override
    public int compareTo(Object arg) {
        DateRange other = (DateRange) arg;
        if (!start.equals(other.start)) return start.compareTo(other.start);
        if (null == this.end) {
            return 1;
        }
        if (null == other.end) {
            return -1;
        }
        return end.compareTo(other.end);
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");

        stringBuilder.append(DATE_FORMAT.format(start));
        stringBuilder.append(",");
        stringBuilder.append(null == end ? "" : DATE_FORMAT.format(end));
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}
