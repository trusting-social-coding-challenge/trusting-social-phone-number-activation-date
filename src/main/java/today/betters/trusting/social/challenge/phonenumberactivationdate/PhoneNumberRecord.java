package today.betters.trusting.social.challenge.phonenumberactivationdate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
@Getter
@AllArgsConstructor
public class PhoneNumberRecord {
    private final String phoneNumber;
    private final Date activationDate;
    private Date deactivationDate;

    public PhoneNumberRecord(String phoneNumber, Date activationDate) {
        this.phoneNumber = phoneNumber;
        this.activationDate = activationDate;
    }

}
