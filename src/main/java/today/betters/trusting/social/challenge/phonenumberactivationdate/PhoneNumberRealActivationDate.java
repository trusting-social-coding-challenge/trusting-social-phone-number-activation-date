package today.betters.trusting.social.challenge.phonenumberactivationdate;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

@Data
public class PhoneNumberRealActivationDate {
    private final String phoneNumber;
    private final Date realActivationDate;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("{");
        sb.append("phoneNumber='").append(phoneNumber).append('\'');
        sb.append(", realActivationDate=").append(DateRange.DATE_FORMAT.format(realActivationDate));
        sb.append('}');
        return sb.toString();
    }
}
