package today.betters.trusting.social.challenge.phonenumberactivationdate;

import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;
import static today.betters.trusting.social.challenge.phonenumberactivationdate.DateRange.DATE_FORMAT;

class DateRangeTest {

    @Test
    void testIncludes() throws ParseException {

        // GIVEN

        DateRange active = new DateRange(DATE_FORMAT.parse("2016-12-01"));

        DateRange rightBefore = new DateRange(DATE_FORMAT.parse("2016-09-01"), DATE_FORMAT.parse("2016-12-01"));

        // WHEN

        boolean isIncluded = active.includes(rightBefore.end());

        // THEN
        assertTrue(isIncluded, rightBefore.toString() + " includes " + active.end() + "");

    }

}
