package today.betters.trusting.social.challenge.phonenumberactivationdate;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static today.betters.trusting.social.challenge.phonenumberactivationdate.DateRange.DATE_FORMAT;

@Slf4j
class PhoneNumberRecordTest {

    @Test
    public void testBaseCase() throws ParseException {
        // GIVEN
        List<PhoneNumberRecord> list = new ArrayList<>();

        list.add(new PhoneNumberRecord("0987000001", DATE_FORMAT.parse("2016-03-01"), DATE_FORMAT.parse("2016-05-01")));
        list.add(new PhoneNumberRecord("0987000002", DATE_FORMAT.parse("2016-02-01"), DATE_FORMAT.parse("2016-03-01")));
        list.add(new PhoneNumberRecord("0987000001", DATE_FORMAT.parse("2016-01-01"), DATE_FORMAT.parse("2016-03-01")));
        list.add(new PhoneNumberRecord("0987000001", DATE_FORMAT.parse("2016-12-01")));
        list.add(new PhoneNumberRecord("0987000002", DATE_FORMAT.parse("2016-03-01"), DATE_FORMAT.parse("2016-05-01")));
        list.add(new PhoneNumberRecord("0987000003", DATE_FORMAT.parse("2016-01-01"), DATE_FORMAT.parse("2016-01-10")));
        list.add(new PhoneNumberRecord("0987000001", DATE_FORMAT.parse("2016-09-01"), DATE_FORMAT.parse("2016-12-01")));
        list.add(new PhoneNumberRecord("0987000002", DATE_FORMAT.parse("2016-05-01")));
        list.add(new PhoneNumberRecord("0987000001", DATE_FORMAT.parse("2016-06-01"), DATE_FORMAT.parse("2016-09-01")));

        Map<String, List<DateRange>> recordsGroupByPhoneNumber = new HashMap<>();
        list.forEach(record -> {
            recordsGroupByPhoneNumber.putIfAbsent(record.getPhoneNumber(), new ArrayList<>());
            List<DateRange> numberDateRanges = recordsGroupByPhoneNumber.get(record.getPhoneNumber());
            numberDateRanges.add(new DateRange(record.getActivationDate(), record.getDeactivationDate()));
        });

        ArrayList<PhoneNumberRealActivationDate> result = new ArrayList<>();

        recordsGroupByPhoneNumber.forEach((p, r) -> {
            Collections.sort(r);
            log.info("For {}, the records are {} ", p, r);
            int latestIndex = r.size() - 1;
            while (latestIndex > 0) {
                if (r.get(latestIndex).includes(r.get(latestIndex-1).end())) {
                    latestIndex--;
                } else {
                    break;
                }
            }
            result.add(new PhoneNumberRealActivationDate(p, r.get(latestIndex).start()));

        });

        result.forEach(r -> {
            log.info(r.toString());
        });
        PhoneNumberRealActivationDate result_0987000001 = result.stream().filter(p -> p.getPhoneNumber().equals("0987000001")).findAny().orElseThrow(() -> new RuntimeException("Can not find record for 0987000001 "));
        assertEquals(result_0987000001.getRealActivationDate(), DATE_FORMAT.parse("2016-06-01"));
        PhoneNumberRealActivationDate result_0987000002 = result.stream().filter(p -> p.getPhoneNumber().equals("0987000002")).findAny().orElseThrow(() -> new RuntimeException("Can not find record for 0987000002 "));
        assertEquals(result_0987000002.getRealActivationDate(), DATE_FORMAT.parse("2016-02-01"));
        PhoneNumberRealActivationDate result_0987000003 = result.stream().filter(p -> p.getPhoneNumber().equals("0987000003")).findAny().orElseThrow(() -> new RuntimeException("Can not find record for 0987000002 "));
        assertEquals(result_0987000003.getRealActivationDate(), DATE_FORMAT.parse("2016-01-01"));

    }

}
